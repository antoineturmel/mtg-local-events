#!/bin/bash

rm planning.html
wget -O planning.html http://lesmagesnazairiens.forumgratuit.org/t348-planning-previsionnel
cat planning.html | grep 'le format' | sed 's/<\/div><\/div><\/div><div class="postprofile.*//' | sed 's/.*le format<br \/><br \/>//' | sed 's/<br \/>/\n/g' | sed 's/<\/font>//g' | sed 's/<font.*">//g' | sed 's/<\/span>//g' > planning.html

echo "[" > events.json

i=0

while read p; do

  if [ $i -gt 0 ]; then
    echo "," >> events.json
  fi
  i=$((i+1))

  month=`cut -d " " -f 2 <<< $p | cut -d "/" -f 2`
  #echo $month
  if [ "$month" -ge 10 -a "$month" -le 12 ]; then year=2016;
  else year=2017; fi
  daymonth=`cut -d " " -f 2 <<< $p`
  daymonthyear=$daymonth"/"$year
  eventname=`cut -d " " --complement -f1 <<< $p | cut -d " " --complement -f1`

  #Format
  case "$eventname" in
  *EDH* )
    format="edh" ;;
  *Modern* )
    format="modern" ;;
  *Peasant* )
    format="peasant" ;;
  *T2* )
    format="standard" ;;
  *Draft* )
    format="draft" ;;
  *AP* )
    format="sealed" ;;
  *"Scellé"* )
    format="sealed" ;;
  esac

  if [ "$format" == "" ]; then
    format="false"
  fi

  #Edition
  case "$eventname" in
  *Kaladesh* )
    edition="KLD" ;;
  *"La révolte éthérique"* )
    edition="AER" ;;
  *"Aether Revolt"* )
    edition="AER" ;;
  *"Modern Masters 2017"* )
    edition="MM3" ;;
  *"modern masters"* )
    edition="MM3" ;;
  *Amonkhet* )
    edition="AKH" ;;
  *"Hour of Devastation"* )
    edition="HOU" ;;
  esac

  if [ "$edition" == "" ]; then
    edition="false"
  fi

  #Tournoi
  case "$eventname" in
  *Tournoi* )
    tournament="true";;
  *TOURNOI* )
    tournament="true";;
  esac

  if [ "$tournament" == "" ]; then
    tournament="false"
  fi

  #Férié
  case "$eventname" in
  *Férié* )
    holiday="true";;
  esac

  if [ "$holiday" == "" ]; then
    holiday="false"
  fi

  echo $daymonthyear $eventname
  echo "format: " $format "edition: " $edition "tournoi: " $tournament "férié: " $holiday

  jq --arg holiday $holiday --arg tournament "$tournament" --arg edition \
  $edition --arg format $format --arg eventname "$eventname" --arg date \
  $daymonthyear '{"holiday":$holiday, "tournament":$tournament, "edition":$edition, "format":$format, "name":$eventname, "date":$date}' <<< 1 >> events.json

  format=""
  edition=""
  holiday=""
  daymonthyear=""
  name=""
  tournament=""

done <planning.html

echo "]" >> events.json

if [ -s planning.html ]
then
 echo "done"
else
 ./generate.sh
fi
